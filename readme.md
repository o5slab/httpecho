# Http Echo

A dead simple web server for debugging. It listens on a port and responses all you send.

## Run

Run the go application directly

```
export HTTP_LISTEN=0.0.0.0:8080
go run main.go
```

Or using docker image

```
docker run --rm -p 8080:8080 -e 'HTTP_LISTEN=0.0.0.0:8080' registry.gitlab.com/o5slab/httpecho:latest
```
