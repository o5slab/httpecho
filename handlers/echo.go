package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/o5slab/httpecho/pb"
	"log"
)

var (
	// Echo ...
	Echo = NewHandlerFactory().GetEchoHandler()
)

// HandlerFactory ...
type HandlerFactory interface {
	GetEchoHandler() gin.HandlerFunc
}

type echo struct {
}

func (e *echo) handle(c *gin.Context) {
	req := new(pb.EchoRequest)
	if err := c.ShouldBindJSON(req); err != nil {
		// in case of error binding request data, just ignore it
		log.Printf("failed to bind request data: %s\n", err)
	}

	if req.WantedCode == 0 {
		req.WantedCode = 200
	}

	c.Header("X-Remote-IP", c.ClientIP())
	c.JSON(int(req.WantedCode), gin.H{"status": "ok"})
}

// GetEchoHandler ...
func (e *echo) GetEchoHandler() gin.HandlerFunc {
	return e.handle
}

// NewHandlerFactory ...
func NewHandlerFactory() HandlerFactory {
	return new(echo)
}
