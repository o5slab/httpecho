FROM golang:1.14 as builder

ARG VERSION=0.0.0
ENV CGO_ENABLED=0 GO111MODULE=on GOARCH=amd64

ENV LISTEN_ADDR=0.0.0.0:80

WORKDIR /src
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY . .

RUN go build -a -ldflags="-s -w -X main.version=$VERSION" -o /app *.go

# ------------ Run time ---------------
FROM alpine:3.11

WORKDIR /svc
COPY --from=builder /app /usr/bin/app

CMD ["/usr/bin/app"]
