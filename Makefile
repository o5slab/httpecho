proto-gen: pb/echo.proto
	protoc -I=. --go_out=. $<

clean:
	rm -f ./pb/echo.pb.go

run:
	go run main.go
