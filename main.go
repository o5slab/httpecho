package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/o5slab/httpecho/handlers"
	"log"
	"net/http"
	"os"
)

func main() {
	listen := "0.0.0.0:8080"
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	listenCfg := os.Getenv("LISTEN_ADDR")
	if "" != listenCfg {
		listen = listenCfg
	}

	router := gin.New()
	router.Any("/", handlers.Echo)
	svr := http.Server{
		Addr:              listen,
		Handler:           router,
	}
	log.Printf("start listening %s\n", listen)
	if err := svr.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
